# Install JDK

## Function
Installs the JDK and JDK Cryptography Policies

## Tested Environment(s)
RHEL 7 & CentOS 7

## Changelog

**v1.0 (2017-03-07)**
* Original Release
